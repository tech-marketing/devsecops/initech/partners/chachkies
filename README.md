# Chackies

Welcome to Chachkies. This little service tells you
how much Flair you need!

## Usage

1. Run the application
```
$ go run main.go
```

2. Open a separate terminal

3. Send a request to the application
```
$ curl 0.0.0.0:8090/flair

You need 100 pieces of flair!
```
