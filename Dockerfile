FROM golang:alpine

RUN apk update
RUN apk add g++ git

COPY ./go.mod /app/go.mod

WORKDIR /app
RUN go mod download
COPY . /app

RUN go build -o /chachkies

EXPOSE 8090
CMD [ "/chachkies" ]
