package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

type FlairService interface {
	Flair() string
}

type flairService struct{}

func (flairService) Flair() string {
	randomInt := rand.Intn(100)
	return fmt.Sprintf("%v pieces!", randomInt)
}

func main() {
	svc := flairService{}
	flairHandler := httptransport.NewServer(
		makeFlairEndpoint(svc),
		decodeFlairRequest,
		encodeResponse,
	)

	http.Handle("/flair", flairHandler)
	log.Fatal(http.ListenAndServe(":8090", nil))
}

func makeFlairEndpoint(svc FlairService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		v := svc.Flair()
		return flairResponse{v}, nil
	}
}

type flairRequest struct {
}

func decodeFlairRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request flairRequest
	return request, nil
}

type flairResponse struct {
	V string `json:"flair"`
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
